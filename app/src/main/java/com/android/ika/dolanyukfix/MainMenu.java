package com.android.ika.dolanyukfix;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MainMenu extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Context mContext;
    private List<Destinasi> mData;


    private BottomNavigationView mMainNav;
    private FrameLayout mFrame;

    private HomeFragment homeFragment;
    private NotifFragment notifFragment;
    private FavFragment favFragment;
    private MeFragment meFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mFrame = (FrameLayout) findViewById(R.id.main_frame);
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);

        homeFragment = new HomeFragment();
        notifFragment = new NotifFragment();
        favFragment = new FavFragment();
        meFragment = new MeFragment();

        setFragment(homeFragment);

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){

                    case R.id.nav_home:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimary);
                        setFragment(homeFragment);
                        return true;

                    case R.id.nav_fav:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(favFragment);
                        return true;

                    case R.id.nav_notif:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimary);
                        setFragment(notifFragment);
                        return true;

                    case R.id.nav_me:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(meFragment);
                        return true;

                    default:
                        return false;

                }
            }



        });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}
